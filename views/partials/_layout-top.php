<!DOCTYPE html>
	<html lang="pl">
		<head>
		    <meta charset="utf-8">
		    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		    <meta name="viewport" content="width=device-width, initial-scale=1">
		    <title>Katalog Moto Equip</title>
		    <link rel="stylesheet" href="assets/css/normalize.css">
		    <link rel="stylesheet" href="assets/css/foundation.css">
		    <link rel="stylesheet" href="assets/css/css.css">
		    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
		    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		    <!--[if lt IE 9]>
		      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		    <![endif]-->
		</head>
		<body>
			<nav class="top-bar" data-topbar="">
				<ul class="title-area">
 					<li class="name">
						<h1>
							<a href="#">
								Moto Equip Web Katalog
							</a>
						</h1>
					</li>
				</ul>
				<section class="top-bar-section">
					<ul class="right">
						<li>
							<div class="nav-search">
								<input type="text"placeholder="Wpisz nazwę lub symbol" />
								<a href="#" class="button tiny">Szukaj</a>
							</div>
						</li>
						<li class="divider">

						</li>
						<li class="divider"></li>
						<li>
							<a href="#">
								Jak sprawdzić dostępność części ?
							</a>
						</li>
						<li class="divider">
						</li>
						<li class="blue">
							<a href="#">Zaloguj się</a>
						</li>
					</ul>
				</section>
			</nav> 

			