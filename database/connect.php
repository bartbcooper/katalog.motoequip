<?php
	require_once 'config.php';

    try {
        $pdo = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
        $pdo->exec('SET NAMES "utf8"');
    } catch (PDOExpection $e) 
    {
        die('Nie można nawiązać połączenia z bazą danych. Błąd'.$e);
    }
    $return = 'Nawiązano połączenie z bazą danych';
