<?php
	ini_set('error_reporting', -1);
	ini_set('display_errors', 1);
	date_default_timezone_set('Europe/Warsaw');
	// to tylko do debugu, pokazywanie bledow
	// nie wrzucaj tego ini_set na serwer, bo jak sie cos wypierdoli to wyswietli wszystko i hakiery sie dobiora :)

	require_once 'database/connect.php';
	// mozesz se tutaj zainkludowac jeszcze jakies inne configi, biblioteki, pliki z funkcjami pomocniczymi (trzymaj je w osobnych katalogach!)
	


	// tutaj znajdujemy sobie ktora strone wyswietlic, po parametrze 'page', czyli w url bedzie:
	// http://mojastrona.local/?page=costam, albo http://mojastrona.local/index.php?page=costam
	// index.php jest opcjonalny, bo to domyslne jest

	if (isset($_GET['page'])) { // musisz sprawdzac czy jest w ogole parametr GET, bo inaczej pierdolnie Ci bledem
		$page = $_GET['page'];

		switch ($page) {
			case 'home':
				include 'controller/home.php';
				break;

			case 'obudowy_kluczykow':
				include 'controller/obudowy_kluczykow.php';
				break;				

			case 'about':
				include 'controller/categories.php';
				break;

			case 'contact':
				include 'controller/contact.php';
				break;
			
			default:
				include 'controller/home.php'; // defaultowo, jesli nie ma zadnego parametru w url wywalamy home
												// zamiast tego mozemy tutaj includowac jakas strone 404
				break;
		}
	} else {
		include 'controller/home.php'; // defaultowo, jesli nie ma zadnego parametru w url wywalamy home
	}

	// normalnie powinno sie zrobic routing i miec ladne url typu http://cos.local/adres/podstrony
	// ale na to przyjdzie Ci jeszcze czas, poleganie na getach jest na poczatku latwiejsze :)
	// no i musisz sie nauczyc jak dziala POST i GET (jest jeszcze PUT i DELETE - ale to na pozniej)




	// i to generalnie tyle w indexie, nie ma co zasmiecac. w zaleznosci jaki mamy parametr, to taka strone sobie dolaczamy
	// a w zasadzie controller tej strony (tam beda wyciagane potrzebne rzeczy z bazy, obrabiane dane i wyswietlany html)
	// (to nie jest naprawde controller w rozumieniu mvc, ale nie mialem lepszego pomyslu na nazwe)
